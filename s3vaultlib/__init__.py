# -*- coding: utf-8 -*-

"""Top-level package for S3Vault Library."""

__author__ = """Giuseppe Chiesa"""
__email__ = 'mail@giuseppechiesa.it'
__application__ = 's3vaultlib'
__version__ = '2.0.6'

